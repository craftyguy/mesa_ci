#!/usr/bin/python2
# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/

from __future__ import print_function
import git
import os
import sys
import time
import urllib

sys.path.append("/var/cache/mesa_jenkins/repos/mesa_ci/services/")
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), ".."))
import util

sys.argv[0] = os.path.abspath(sys.argv[0])

WORKSPACE = '/tmp/fetch_skqp_assets'
SKQP_REPO = '/var/lib/git/skqp'
# Track skqp/dev assets to try and get ahead of any changes before they hit
# skqp/release branch
SKQP_BRANCH = 'origin/skqp/dev'
UPSTREAM_ASSET_REPO = 'git@gitlab.freedesktop.org:Mesa_CI/skqp_assets.git'
UPSTREAM_ASSET_URL = 'https://storage.googleapis.com/skia-skqp-assets'


def setup_repo(remote, path, branch='origin/master'):
    """ Fetch from given remote, if local repo does not exist then clone it.
    Finally, check out given branch and return git.Repo object for repo """
    try:
        new_repo = git.Repo(path)
    except (git.NoSuchPathError, git.InvalidGitRepositoryError):
        new_repo = git.Repo.clone_from(remote, path)
    new_repo.git.fetch()
    new_repo.git.checkout(branch)

    return new_repo


def get_asset(url, path, md5):
    """ Download asset from url, write to path, and verify md5 checksum. """
    # strip out filename from path
    asset_dir = '/'.join(path.split('/')[:-1])
    if not os.path.exists(asset_dir):
        os.makedirs(asset_dir)
    tries = 1

    # if asset is already on disk and checksum didn't change, don't fetch it
    if os.path.exists(path):
        checksum = util.file_checksum(path)
        if checksum == md5:
            return

    while tries <= 3:
        try:
            print('Attempting to fetch asset: {}'.format(path))
            urllib.urlretrieve(url, path)
            checksum = util.file_checksum(path)
            if checksum == md5:
                return
        except IOError:
            pass
        print("WARN: Unable to fetch asset: {}. Attempt {}/3".format(path,
                                                                     tries))
        tries += 1
        time.sleep(3)
    raise RuntimeError("ERROR: Unable to fetch asset: {}".format(path))


def main():
    if not os.path.exists(WORKSPACE):
        os.makedirs(WORKSPACE)
    filelist_path = WORKSPACE + '/filelist'
    assets_repo_path = WORKSPACE + '/skqp_assets'
    skqp_repo_path = WORKSPACE + '/skqp'
    assets_repo = setup_repo(UPSTREAM_ASSET_REPO, assets_repo_path)
    skqp_repo = setup_repo(SKQP_REPO, skqp_repo_path, SKQP_BRANCH)
    fc_path = (skqp_repo_path + '/platform_tools/android/apps/skqp/src'
               + '/main/assets/files.checksum')

    while True:
        skqp_repo.remotes['origin'].fetch()
        skqp_repo.git.checkout(SKQP_BRANCH)

        with open(fc_path, 'r') as fc_fh:
            upstream_assets_ver = fc_fh.readline().rstrip()

        if upstream_assets_ver in assets_repo.tags:
            continue
        print('INFO: Found new version for assets')
        if 'update' in assets_repo.branches:
            assets_repo.git.branch('-D', 'update')
        assets_repo.git.fetch()
        assets_repo.git.checkout('origin/master', b='update')
        get_asset(UPSTREAM_ASSET_URL + '/' + upstream_assets_ver,
                  filelist_path, upstream_assets_ver)
        # read new asset file list and fetch each asset
        with open(filelist_path, 'r') as f:
            assets = f.readlines()

        for asset in assets:
            md5, path = asset.split(';')
            path = path.rstrip()
            get_asset(UPSTREAM_ASSET_URL + '/' + md5,
                      assets_repo_path + '/' + path, md5)

        # Add new/changed files, create new tag, and push
        assets_repo.git.add('.')
        assets_repo.git.commit('-m',
                               'Updating assets due to ' + upstream_assets_ver)
        assets_repo.create_tag(upstream_assets_ver)
        assets_repo.git.push('origin', 'update:master')
        assets_repo.git.push('origin', upstream_assets_ver)
        # reset to origin/master and delete the update branch
        assets_repo.git.checkout('origin/master')
        assets_repo.git.branch('-D', 'update')

        # Poll for new assets once a day
        time.sleep(86400)


if __name__ == "__main__":
    main()
