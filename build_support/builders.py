# Copyright (C) Intel Corp.  2014.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Mark Janes <mark.a.janes@intel.com>
#  **********************************************************************/
import multiprocessing
import os
import re
import shutil
import socket
import subprocess
import sys
import importlib
import git
import glob
import time
import urllib2
import xml.etree.cElementTree as et
from . import Options
from . import ProjectMap
from . import run_batch_command
from . import rmtree
from . import Export
from . import GTest
from . import RepoSet
from . import ProjectInvoke
from . import Jenkins
from . import RevisionSpecification
from . import get_conf_file
from . import TestLister
from . import NoConfigFile


def mesa_version(env=None):
    br = ProjectMap().build_root()
    wflinfo =  br + "/bin/wflinfo"
    if not env:
        env = {}
    env.update({
        'LD_LIBRARY_PATH': ':'.join([
            get_libdir(),
            get_libgl_drivers(),
            os.path.join(br, 'lib', 'piglit', 'lib'),
        ]),
        "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
    })
    (out, _) = run_batch_command([wflinfo,
                                 "--platform=gbm", "-a", "gl"],
                                 streamedOutput=False, env=env)
    for a_line in out.splitlines():
        if "OpenGL version string" not in a_line:
            continue
        tokens = a_line.split(":")
        assert len(tokens) == 2
        version_string = tokens[1].strip()
        version_tokens = version_string.split()
        assert len(version_tokens) >= 3
        return version_tokens[2]

def cpu_count():
    cpus = multiprocessing.cpu_count() + 1
    if cpus > 18:
        cpus = 18
    return cpus


def _system_dirs():
    """Returns the correct lib prefix for debian vs non-debian."""
    lib_dirs = ['lib']
    if Options().arch == "m32":
        if os.path.exists('/etc/debian_version'):
            lib_dirs.append("lib/i386-linux-gnu")
    else:
        lib_dirs.append("lib64")
        if os.path.exists('/etc/debian_version'):
            lib_dirs.append("lib/x86_64-linux-gnu")
    return lib_dirs


def get_package_config_path():
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l, 'pkgconfig') for l in lib_dirs] +
        [os.path.join('/usr', l, 'pkgconfig') for l in lib_dirs] +
        ["/usr/lib/pkgconfig"])


def get_libgl_drivers():
    """Get the correct drivers dir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l, 'dri') for l in lib_dirs] +
        [os.path.join('/usr', l, 'dri') for l in lib_dirs])


def get_libdir():
    """Get the correct libdir depending on platform and arch."""
    lib_dirs = _system_dirs()
    build_root = ProjectMap().build_root()
    return ':'.join(
        [os.path.join(build_root, l) for l in lib_dirs] +
        [os.path.join('/usr', l) for l in lib_dirs])


def delete_src_pyc(path):
    for dirpath, _, filenames in os.walk(path):
        for each_file in filenames:
            if each_file.endswith('.pyc'):
                if os.path.exists(os.path.join(dirpath, each_file)):
                    os.remove(os.path.join(dirpath, each_file))

def git_clean(src_dir):
    savedir = os.getcwd()
    os.chdir(src_dir)
    run_batch_command(["git", "clean", "-xfd"])
    run_batch_command(["git", "reset", "--hard", "HEAD"])
    os.chdir(savedir)

def check_gpu_hang(identify_test=True):
    # some systems have a gpu hang watchdog which reboots
    # machines, and others do not.   This method checks dmesg,
    # produces a failing test if a hang is found, and schedules a
    # reboot if the host is determined to be a jenkins builder
    # (user=jenkins)
    if os.name == "nt":
        return
    try:
        (out, _) = run_batch_command(["dmesg", "--time-format", "iso"],
                                     quiet=True,
                                     streamedOutput=False)
    except:
        return
    hang_text = ""
    for a_line in out.split('\n'):
        if "gpu hang" in a_line.lower():
            hang_text = a_line
            break
        if "*error* ring create req" in a_line.lower():
            hang_text = a_line
            break
        if "unable to purge gpu memory due lock contention" in a_line.lower():
            hang_text = a_line
            break
    if not hang_text:
        return False

    print "ERROR: gpu hang found: " + hang_text
    print "ERROR: system must be rebooted."

    # obtain the pid from the hang_text
    br = ProjectMap().build_root()
    m = re.search(r"\[([0-9]+)\]", hang_text)
    if m is not None and identify_test:
        pid = m.group(1)
        test_path = os.path.abspath(br + "/../test")
        test = TestLister(test_path, include_passes=True).TestForPid(pid)
        if test is not None:
            hang_text += "\nHanging Test:\n" + test
        
    hostname = socket.gethostname()
    Export().create_failing_test("gpu-hang-" + hostname,
                                 hang_text)
    test_path = os.path.abspath(br + "/../test/")
    if not os.path.exists(test_path):
        os.makedirs(test_path)
    try:
        run_batch_command(["sudo", "/usr/local/bin/copy_error_state.sh", test_path + "/card_error_" + hostname, "jenkins"])
    except:
        print "WARN: failed to capture error state"
    
    # trigger reboot
    if ('otc-gfx' in hostname):
        label = hostname[len('otc-gfxtest-'):]
        server = ProjectMap().build_spec().find("build_master").attrib["host"]
        url = "http://" + server + "/job/reboot_single/buildWithParameters?token=noauth&label=" + label
        print "opening: " + url
        urllib2.urlopen(url)
        print "sleeping to allow reboot job to be scheduled."
        time.sleep(120)
    return True

class AutoBuilder(object):

    def __init__(self, o=None, configure_options=None, export=True,
                 opt_flags="", install=True):
        self._options = o
        self._tests = None
        self._export = export
        self._opt_flags = opt_flags
        self.install = install

        self._configure_options = configure_options
        if not configure_options:
            self._configure_options = []

        if not o:
            self._options = Options()

        self._project_map = ProjectMap()
        project = self._project_map.current_project()
        self._project = project

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = self._project_map.build_root()
        self._build_dir = self._src_dir + "/build_" + self._options.arch

        self._env = {}
        self._options.update_env(self._env)

    def build(self):
        if not os.path.exists(self._build_root):
            os.makedirs(self._build_root)
        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)

        optflags = self._opt_flags
        if self._options.config != "debug":
            optflags = "-O2 -DNDEBUG"
            
        savedir = os.getcwd()
        pkg_config = get_package_config_path()
        os.chdir(self._build_dir)
        flags = []
        if self._options.arch == "m32":
            flags = ["CFLAGS=-m32 -msse -msse2 " + optflags,
                     "CXXFLAGS=-m32 -msse -msse2 " + optflags, 
                     "--enable-32-bit",
                     "--host=i686-pc-linux-gnu"]
        else:
            flags = ["CFLAGS=-m64 " + optflags,
                     "CXXFLAGS=-m64 " + optflags]

        os.chdir(self._src_dir)
        run_batch_command(["autoreconf", "--verbose", "--install", "-s"], env=self._env)
        os.chdir(self._build_dir)

        env = self._env.copy()
        env.update({
            "PKG_CONFIG_PATH": pkg_config,
            "LD_LIBRARY_PATH": get_libdir(),
            "CC": "ccache gcc -" + self._options.arch,
            "CXX": "ccache g++ -" + self._options.arch,
        })

        run_batch_command([self._src_dir + "/configure", 
                           "--prefix=" + self._build_root] + \
                          flags + self._configure_options, env=env)

        run_batch_command(["make",  "-j", 
                           str(cpu_count())], env=self._env)
        if self.install:
            run_batch_command(["make",  '-j', str(cpu_count()), "install"],
                              env=self._env)

        os.chdir(savedir)

        if self._export:
            Export().export()

    def SetGtests(self, tests):
        self._tests = GTest(binary_dir = self._build_dir, executables=tests)

    def test(self):
        savedir = os.getcwd()
        os.chdir(self._build_dir)

        try:
            run_batch_command(["make",  "-k", "-j", 
                               str(cpu_count()),
                               "check"], env=self._env)
        except(subprocess.CalledProcessError):
            print "WARN: make check failed"
            os.chdir(savedir)
            # bug 91773
            # Export().create_failing_test(self._project +
            #                              "-make-check-failure", "")
        os.chdir(savedir)

        if self._tests:
            self._tests.run_tests()

        if self._export:
            Export().export()

    def clean(self):
        git_clean(self._src_dir)
        if self._build_dir != self._src_dir:
            rmtree(self._build_dir)
            assert(not os.path.exists(self._build_dir))

class CMakeBuilder(object):
    def __init__(self, extra_definitions=None, compiler="gcc", install=True):
        self._options = Options()
        self._project_map = ProjectMap()
        self._compiler = compiler
        self._install = install

        if not extra_definitions:
            extra_definitions = []
        self._extra_definitions = extra_definitions

        project = self._project_map.current_project()

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = self._project_map.build_root()
        self._build_dir = self._src_dir + "/build_" + self._options.arch

    def build(self):

        if not os.path.exists(self._build_dir):
            os.makedirs(self._build_dir)

        pkg_config = get_package_config_path()
        savedir = os.getcwd()
        os.chdir(self._build_dir)
        env = {
            'PKG_CONFIG_PATH': get_package_config_path(),
            'CC': 'ccache {}'.format('gcc' if self._compiler != 'clang' else 'clang'),
            'CXX': 'ccache {}'.format('g++' if self._compiler != 'clang' else 'clang++'),
            'CFLAGS': '-m64' if self._options.arch == 'm64' else '-m32',
            'CXXFLAGS': '-m64' if self._options.arch == 'm64' else '-m32',
            # Meson does not set an rpath by default, that is project
            # decicision (CMake does the same, but Autotools does set rpath by
            # default). To work around this for projects (like mesa) that
            # don't, we set the LD_LIBRARY_PATH, This avoids problems like
            # libEGL trying to open an older system libgm.
           'LD_LIBRARY_PATH': get_libdir(),
        }
        self._options.update_env(env)
        run_batch_command(["cmake", "-GNinja", self._src_dir, 
                           "-DCMAKE_INSTALL_PREFIX:PATH=" + self._build_root] \
                          + self._extra_definitions, env=env)

        run_batch_command(["ninja", "-j" + str(cpu_count())], env=env)
        if self._install:
            print "Installing: output suppressed"
            run_batch_command(["ninja", "install"], streamedOutput=False, quiet=True)

        os.chdir(savedir)

        Export().export()

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert(not os.path.exists(self._build_dir))
        
    def test(self):
        savedir = os.getcwd()
        os.chdir(self._build_dir)

        env = {}
        self._options.update_env(env)

        # get test names
        command = ["ctest", "-V", "-N"]
        (out, _) = run_batch_command(command, streamedOutput=False, quiet=True, env=env)

        os.chdir(savedir)

        out = out.splitlines()
        for aline in out:
            # execute each command reported by ctest
            match = re.match(".*Test command: (.*)", aline)
            if not match:
                continue
            (bin_dir, exe) = os.path.split(match.group(1))
            #bs.GTest(bin_dir, exe, working_dir=bin_dir).run_tests()

class MesonBuilder(object):

    def __init__(self, extra_definitions=None, compiler="gcc", install=True, cpp_args=None):
        self._options = Options()
        self._project_map = ProjectMap()
        self._compiler = compiler
        self._install = install
        self._extra_definitions = extra_definitions or []
        self._cpp_args = cpp_args
        self.tests = [] # List of tests to run
        self.gtests = [] 

        project = self._project_map.current_project()

        self._src_dir = self._project_map.project_source_dir(project)
        self._build_root = self._project_map.build_root()
        self._build_dir = os.path.join(
            self._src_dir, '_'.join(['build', project, self._options.arch]))

    def build(self):
        if not os.path.exists(os.path.join(self._src_dir, "meson.build")):
            return
        env = {
            'PKG_CONFIG_PATH': get_package_config_path(),
            'CC': 'ccache {}'.format('gcc' if self._compiler != 'clang' else 'clang'),
            'CXX': 'ccache {}'.format('g++' if self._compiler != 'clang' else 'clang++'),
            # Meson does not set an rpath by default, that is project
            # decicision (CMake does the same, but Autotools does set rpath by
            # default). To work around this for projects (like mesa) that
            # don't, we set the LD_LIBRARY_PATH, This avoids problems like
            # libEGL trying to open an older system libgm.
           'LD_LIBRARY_PATH': get_libdir(),
        }
        if self._cpp_args:
            env["CPPFLAGS"] = self._cpp_args

        self._options.update_env(env)

        returnto = os.getcwd()
        os.chdir(self._src_dir)

        # Set libdir to make deqp happy on debian, since otherwise the GLES
        # header dedection will fail.
        if self._options.arch == 'm32':
            cross_file = ['--cross-file', os.path.join(
                os.path.dirname(__file__),
                'x86-linux-gcc.cross' if self._compiler != 'clang' else 'x86-linux-clang.cross')]
        else:
            cross_file = []
        cmd = ['meson', self._build_dir, '--prefix', self._build_root,
                           '--libdir', 'lib'] + cross_file + self._extra_definitions
        if os.path.exists(self._build_dir):
            cmd.append('--reconfigure')
        run_batch_command(cmd,
                           env=env)
        run_batch_command(['ninja', '-j', str(cpu_count()), '-C', self._build_dir])
        if self._install:
            print "Installing: output suppressed"
            run_batch_command(['ninja', '-C', self._build_dir, 'install'],
                              streamedOutput=False, quiet=True)

        os.chdir(returnto)

        Export().export()

    def test(self):
        if not os.path.exists(os.path.join(self._src_dir, "meson.build")):
            return
        results_dir = os.path.abspath(os.path.join(
            self._project_map.build_root(), "../test"))
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)

        root = et.Element('testsuites')
        suite = et.SubElement(
            root,
            'testsuite',
            name=self._project_map.current_project(),
            tests=str(len(self.tests)))

        for args in self.tests:
            if isinstance(args, list):
                name = args[0]
            else:
                name = args
                args = [args]

            p = subprocess.Popen(
                ['meson', 'test', '-v', '-C', self._build_dir] + args,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE)
            out, err = p.communicate()
            case = et.SubElement(
                suite,
                'testcase',
                classname="unittest",
                name=name,
                time="0") # TODO
            o = et.SubElement(case, 'system-out')
            o.text = out
            e = et.SubElement(case, 'system-err')
            e.text = err
            if p.returncode != 0:
                et.SubElement(case, 'failure')

        filename = os.path.join(
            results_dir,
            '_'.join(['unittest', self._options.config, self._options.arch,
                      self._options.hardware]) + '.xml')
        tree = et.ElementTree(root)
        tree.write(filename, encoding='utf-8', xml_declaration=True)

        for name in self.gtests:
            filename = os.path.join(
                results_dir,
                '_'.join(['gtest', name, self._options.config,
                          self._options.arch, self._options.hardware]) + '.xml')
            try:
                run_batch_command(['meson', 'test', '-C', self._build_dir, name,
                                   '--test-args',
                                   '"--gtest_output=xml:{}"'.format(filename)])
            except subprocess.CalledProcessError:
                Export().create_failing_test(
                    'failing-gtest-{}'.format(name),
                    'WARN: gtest returned non-zero status: {}'.format(name))
            else:
                if not os.path.exists(filename):
                    Export().create_failing_test(
                        'silent-gtest-{}'.format(name),
                        'ERROR: gtest produced no output: {}'.format(name))

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert not os.path.exists(self._build_dir)


