import gzip
import multiprocessing
import os
import sys
import re
import xml.etree.cElementTree as et
from . import Export
from . import NoConfigFile
from . import Options
from . import PiglitTest
from . import ProjectMap
from . import RepoSet
from . import TestLister
from . import check_gpu_hang
from . import export
from . import get_conf_file
from . import get_libdir
from . import get_libgl_drivers
from . import mesa_version
from . import run_batch_command
from . import rmtree

class PiglitLister:
    def __init__(self, suite=None):
        self._suite = suite
        self._tests = {}
        if not suite:
            return
        assert(type(suite) == type([]))
        self._test_dir = ProjectMap().build_root() + "/lib/piglit/tests/"
        assert(os.path.exists(self._test_dir))
        for a_suite in suite:
            meta_file = self._test_dir + a_suite + ".meta.xml"
            if (os.path.exists(meta_file)):
                meta = et.parse(meta_file)
                for profile in meta.findall("Profile"):
                    self.add(PiglitLister([profile.text]))
            else:
                profile_file = self._test_dir + a_suite + ".xml.gz"
                if not os.path.exists(profile_file):
                    # vkrunner profile may not be available on stable
                    # branches.
                    return
                infile = gzip.open(profile_file, 'rb')
                profile = et.parse(infile)
                for test in profile.findall("Test"):
                    name = test.attrib["name"]
                    name = name.replace(".", "_")
                    name = name.replace("@", ".")
                    name = name.replace(":", ".")
                    name = name.replace("=", ".")
                    name = "piglit." + name
                    self._tests[name] = test

    def filter(self, blacklist):
        if str == type(blacklist):
            blacklist = [blacklist]
        if type(blacklist) == type(PiglitLister("")):
            blacklist = blacklist._tests.keys()
        for test in blacklist:
            test = test.replace(":", ".")
            if not test:
                continue
            if test.startswith("#"):
                continue
            if test in self._tests:
                del self._tests[test]

    def whitelist(self, whitelist):
        for (test, _) in self._tests.items():
            if test not in whitelist:
                del self._tests[test]

    def add(self, testlist):
        self._tests.update(testlist._tests)

    def write_suite(self, target_suite, shard=None):
        test_count = len(self._tests)
        if shard == "0":
            shard = None
        if shard:
            (shard_num, shards) = shard.split(":")
            shard_num = int(shard_num)
            shards = int(shards)
            shard_count = test_count / shards
            if shard_num <= test_count % shards:
                shard_count += 1
            test_count = shard_count
            shard = 1
        with gzip.open(self._test_dir + '{}.xml.gz'.format(target_suite), 'wb') as f:
            f.write("""<?xml version='1.0' encoding='utf-8'?>
<PiglitTestList count="{}" name="{}">\n""".format(test_count, target_suite))
            for (_, tag) in sorted(self._tests.items()):
                if not shard or shard == shard_num:
                    f.write(et.tostring(tag) + "\n")
                if shard:
                    shard += 1
                    if shard > shards:
                        shard = 1
            f.write("</PiglitTestList>")
    
class PiglitTester(object):
    def __init__(self, _suite=None, device_override=None, piglit_test=None,
                 env=None, timeout=None, jobs=None):
        if not _suite:
            _suite = ["quick", "vulkan"]
        if type(_suite) != type([]):
            # suite must be a list
            _suite = [_suite]
        self.jobs = jobs
        if not self.jobs:
            self.jobs = multiprocessing.cpu_count()
        self.device_override = device_override
        self._piglit_test = None
        if piglit_test:
            # drop the hw/arch suffix
            self._piglit_test = ".".join(piglit_test.split(".")[:-1])

        o = Options()
        self.suite = _suite

        # in bisect or single_test, a test may be in either the cpu or gpu suite.
        # use the quick suite, which is more comprehensive
        if o.retest_path or piglit_test:
            self.suite = ["quick", "vulkan"]

        pm = ProjectMap()
        self.build_root = pm.build_root()
        if o.arch == "m64":
            icd_name = "intel_icd.x86_64.json"
        elif o.arch == "m32":
            icd_name = "intel_icd.i686.json"
        self.env = {
            'LD_LIBRARY_PATH': ':'.join([
                get_libdir(),
                os.path.join(get_libdir(), 'dri'),
                os.path.join(self.build_root, 'lib', 'piglit', 'lib'),
            ]),
            "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
            "VK_ICD_FILENAMES" : pm.build_root() + "/share/vulkan/icd.d/" + icd_name,
             # In the event of a piglit related bug, we want the backtrace
             "PIGLIT_DEBUG": "1",

             # Set the path to include buildroot/bin so fast skipping works
             'PATH': ':'.join([
                 os.path.join(self.build_root, 'bin'),
                 os.environ['PATH'],
             ]),
        }
        if env:
            self.env.update(env)
        self.timeout = timeout

        if "hsw" in o.hardware or "byt" in o.hardware or "ivb" in o.hardware:
            self.env["MESA_GLES_VERSION_OVERRIDE"] = "3.1"

    def test(self):
        pm = ProjectMap()
        o = Options()

        mv = mesa_version(self.env)

        dev_ids = { "byt" : "0x0F32",
                    "g45" : "0x2E22",
                    "g965" : "0x29A2",
                    "ilk" : "0x0042",
                    "ivbgt2" : "0x0162",
                    "snbgt2" : "0x0122",
                    "hswgt3" : "0x042A",
                    "bdwgt2" : "0x161E",
                    "chv" : "0x22B0",
                    "sklgt3" : "0x192B",
                    "bsw" : "0x22B1",
        }
        if self.device_override:
            self.env["INTEL_DEVID_OVERRIDE"] = dev_ids[self.device_override]

        o.update_env(self.env)

        out_dir = self.build_root + "/test/" + o.hardware

        hardware = o.hardware
        project = pm.current_project()
        if self.device_override:
            hardware = self.device_override
        try:
            conf_file = get_conf_file(hardware, o.arch, project=project)
        except NoConfigFile:
            print >>sys.stderr, 'No config file found for hardware: {0} arch: {1}'.format(
                hardware, o.arch)
            sys.exit(1)
        
        suffix = o.hardware
        hardware = o.hardware
        if self.device_override:
            suffix = self.device_override
            hardware = self.device_override
        if "gt" in hardware:
            hardware = hardware[:3]
        cmd = [self.build_root + "/bin/piglit",
               "run",
               "-o",
               "-p", "gbm",
               "-b", "junit",
               "--junit_suffix", "." + suffix + o.arch,
               "--config", conf_file,
               "--jobs", str(self.jobs),
        ]
        if self.timeout:
            cmd += ['--timeout', str(self.timeout)]
        target_suite = PiglitLister(self.suite)
        conf_dir = pm.source_root() + "/piglit-test/"
        blacklist = [line.rstrip() for line in open(conf_dir + "blacklist.conf")]
        target_suite.filter(blacklist)
        if o.arch == "m32":
            blacklist = [line.rstrip() for line in open(conf_dir + "m32_blacklist.conf")]
            target_suite.filter(blacklist)
            
        platform_blacklist = conf_dir + hardware + "_blacklist.conf"
        if os.path.exists(platform_blacklist):
            blacklist = [line.rstrip() for line in open(platform_blacklist)]
            target_suite.filter(blacklist)

        internal_conf_dir = pm.source_root() + "/repos/mesa_ci_internal/piglit-test-internal/"
        internal_blacklist = internal_conf_dir + "blacklist.conf"
        if os.path.exists(internal_blacklist):
            blacklist = [line.rstrip() for line in open(internal_blacklist)]
            target_suite.filter(blacklist)
        internal_platform_blacklist = internal_conf_dir + hardware + "_blacklist.conf"
        if os.path.exists(internal_platform_blacklist):
            blacklist = [line.rstrip() for line in open(internal_platform_blacklist)]
            target_suite.filter(blacklist)

        if "18.1" in mv:
            target_suite.filter([line.rstrip() for line in open(conf_dir + "18.1_blacklist.conf")])
        
        if o.retest_path:
            # only test items which previously failed
            test_project = "piglit-test"
            if hardware in ["tgl", "ats"]:
                test_project = "piglit-test-internal"
            include_tests = TestLister(o.retest_path + "/test/").RetestIncludes(test_project)
            if not include_tests:
                # we were supposed to retest failures, but there were none
                return
            target_suite.whitelist(include_tests)

        if self._piglit_test:
            # support for running a single test
            target_suite.whitelist([self._piglit_test])

        target_suite.write_suite("target_suite", o.shard)

        concurrency_options = ["-c"]
        if "DEQP_DETECT_GPU_HANG" in self.env:
            concurrency_options = ["-1", "-v"]
            
        streamedOutput = True
        if o.retest_path:
            streamedOutput = False
        (out, err) = run_batch_command(cmd + concurrency_options + ["target_suite", out_dir ],
                                       env=self.env,
                                       expected_return_code=None,
                                       streamedOutput=streamedOutput)
        if err and "There are no tests scheduled to run" in err:
            open(out_dir + "/results.xml", "w").write("<testsuites/>")

        single_out_dir = self.build_root + "/../test"
        if not os.path.exists(single_out_dir):
            os.makedirs(single_out_dir)

        final_file = single_out_dir + "_".join(["/" + pm.current_project(),
                                                hardware,
                                                o.arch, o.shard]) + ".xml"
        if os.path.exists(out_dir + "/results.xml"):
            # obtain the set of revisions on master which are not on
            # the current branches.  This set represents the revisions
            # that should cause tests to be disregared.
            revisions = RepoSet().branch_missing_revisions()
            print "INFO: filtering tests from " + out_dir + "/results.xml"
            # Uniquely name all test files in one directory, for
            # jenkins
            self.filter_tests(revisions,
                              out_dir + "/results.xml",
                              final_file)

        if "bsw" == hardware:
            # run piglit again, to eliminate intermittent failures
            tl = TestLister(final_file)
            retests = tl.RetestIncludes("piglit-test")
            if retests:
                second_out_dir = out_dir + "/retest"
                target_suite.whitelist(retests)
                target_suite.write_suite("retest")
                print "WARN: retesting piglit to " + second_out_dir
                (out, err) = run_batch_command(cmd + 
                                               concurrency_options + ["retest", second_out_dir ],
                                               env=self.env,
                                               expected_return_code=None,
                                               streamedOutput=streamedOutput)
                second_results = TestLister(second_out_dir + "/results.xml")
                for a_test in tl.TestsNotIn(second_results):
                    print "stripping flaky test: " + a_test.test_name
                    a_test.ForcePass(final_file)
                rmtree(second_out_dir)

        # create a copy of the test xml in the source root, where
        # jenkins can access it.
        cmd = ["cp", "-a",
               self.build_root + "/../test", pm.source_root()]
        run_batch_command(cmd)

        check_gpu_hang()
        Export().export_tests()

    def filter_tests(self, revisions, infile, outfile):
        """this functionality has been duplicated in deqp-test/build.py.  If
        it needs to change, then either change it everywhere or refactor out
        the duplication."""
        t = et.parse(infile)
        r = t.getroot()
        for a_suite in t.findall("testsuite"):
            # remove skipped tests, which uses ram on jenkins when
            # displaying and provides no value.  
            for a_skip in a_suite.findall("testcase/skipped/.."):
                if a_skip.attrib["status"] in ["crash", "fail"]:
                    continue
                a_suite.remove(a_skip)

            # for each failure, see if there is an entry in the config
            # file with a revision that was missed by a branch
            for afail in a_suite.findall("testcase/failure/..") + a_suite.findall("testcase/error/.."):
                piglit_test = PiglitTest("foo", "foo", afail)
                regression_revision = piglit_test.GetConfRevision()
                abbreviated_revisions = [a_rev[:6] for a_rev in revisions]
                for abbrev_rev in abbreviated_revisions:
                    if abbrev_rev in regression_revision:
                        print "stripping: " + piglit_test.test_name + " " + regression_revision
                        a_suite.remove(afail)
                        # a test may match more than one revision
                        # encoded in a comment
                        break

            # strip unneeded output from passing tests
            for apass in a_suite.findall("testcase"):
                if apass.attrib["status"] != "pass":
                    continue
                if apass.find("failure") is not None:
                    continue
                out_tag = apass.find("system-out")
                if out_tag is not None:
                    apass.remove(out_tag)
                err_tag = apass.find("system-err")
                if err_tag is not None and err_tag.text is not None:
                    found = False
                    for a_line in err_tag.text.splitlines():
                        m = re.match("pid: ([0-9]+)", a_line)
                        if m is not None:
                            found = True
                            err_tag.text = a_line
                            break
                    if not found:
                        apass.remove(err_tag)
                
        t.write(outfile)

    def build(self):
        pass

    def clean(self):
        pass
