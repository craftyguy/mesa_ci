#!/usr/bin/python2
import os
import sys
from collections import namedtuple
from . import Options
from . import ProjectMap


class Fulsim(object):

    def __init__(self):
        # Platform config
        # fulsim_ver - a named directory under ~/fulsim/<platform>, and must
        # exist prior to running
        # key_file - a file under the mesa repo for determining whether or not
        # the mesa branch supports this platform
        self.platform_config = namedtuple('platform', 'fulsim_ver key_file')
        self.platform_configs = {
            'tgl': self.platform_config(fulsim_ver='100988',
                                        key_file='src/intel/genxml/gen12.xml'),
            'ats': self.platform_config(fulsim_ver='22493',
                                        key_file='src/intel/genxml/gen12.xml'),
        }
        self._hardware = Options().hardware
        self._arch = Options().arch
        self._project_map = ProjectMap()
        self._build_root = self._project_map.build_root()
        self._mesa_repo_dir = self._project_map.source_root() + "/repos/mesa/"

    def is_supported(self):
        """ Determines if the hardware is supported for running on fulsim """
        if self._hardware not in self.platform_configs:
            print("Environment check failure: Unable to find the expected "
                  "fulsim version for this platform.")
            return False
        # Check for existence of the keyfile if there is one
        if self.platform_configs[self._hardware].key_file:
            if not os.path.exists(self._mesa_repo_dir + "/"
                                  + self.platform_configs[self._hardware].key_file):
                print("Environment check failure: An unsupported Mesa version "
                      "was detected. Make sure the correct Mesa sha was "
                      "specified.")
                return False
        # 32-bit only
        if self._arch == "m32":
            print("Environment check failure: 32-bit is not supported.")
            return False
        return True

    def get_env(self):
        env = {}
        # Additional configuration if running in simulation
        if self._hardware in self.platform_configs:
            sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])),
                                         "..", "repos", "sim-drm"))
            import sim_drm
            aubload = os.path.expanduser('~/fulsim/' + self._hardware + "/"
                                         + self.platform_configs[self._hardware].fulsim_ver
                                         + "/AubLoad")
            env = sim_drm.get_env(self._hardware, aubload=aubload,
                                  libsim_drm=self._build_root + '/lib/libsim-drm.so')
        return env


