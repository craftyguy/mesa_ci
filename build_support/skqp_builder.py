# Copyright (C) Intel Corp.  2018.  All Rights Reserved.

# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:

# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE COPYRIGHT OWNER(S) AND/OR ITS SUPPLIERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

#  **********************************************************************/
#  * Authors:
#  *   Clayton Craft <clayton.a.craft@intel.com>
#  **********************************************************************/
from __future__ import print_function
import ConfigParser
import git
import glob
import importlib
import json
import multiprocessing
import os
import subprocess
import sys
import tempfile
import time
import xml.etree.cElementTree as et
from . import Options
from . import ProjectMap
from . import run_batch_command
from . import rmtree
from . import Export
from . import GTest
from . import RepoSet
from . import get_conf_file
from . import get_libdir
from . import get_libgl_drivers
from . import NoConfigFile
from . import is_build_lab
from . import external_to_repo
from . import repo_to_external
from . import checkout_externals
from . import mesa_version


def cpu_count():
    cpus = multiprocessing.cpu_count() + 1
    if cpus > 18:
        cpus = 18
    return cpus


def git_clean(src_dir):
    savedir = os.getcwd()
    os.chdir(src_dir)
    run_batch_command(['git', 'clean', '-xfd'])
    run_batch_command(['git', 'reset', '--hard', 'HEAD'])
    os.chdir(savedir)


def skqp_get_assets_ver():
    project = ProjectMap().current_project()
    fc_path = (ProjectMap().project_source_dir(project)
               + '/platform_tools/android/apps/skqp/src'
               + '/main/assets/files.checksum')
    with open(fc_path, 'r') as f:
        assets_ver = f.readline().rstrip()
    return assets_ver


def skqp_external_revisions(project, revisions_dict=None):
    if revisions_dict is None:
        revisions_dict = {}
    project = ProjectMap().current_project()
    src_dir = ProjectMap().project_source_dir(project)
    deps_file = src_dir + '/DEPS'
    deps = skqp_get_deps_from_file(deps_file)
    for orig_dep_name, url in deps.items():
        dep_name = orig_dep_name.split('/')[-1]
        revision = url.split('@')[-1]
        if dep_name in external_to_repo:
            dep_name = external_to_repo[dep_name]
        if dep_name in revisions_dict:
            revisions_dict[dep_name] = [revisions_dict[dep_name], revision]
        else:
            revisions_dict[dep_name] = revision
    assets_repo = git.Repo(src_dir + '/../skqp_assets')
    assets_ver = skqp_get_assets_ver()
    revisions_dict['skqp_assets'] = assets_repo.tags[assets_ver].commit.hexsha
    return revisions_dict


class SkqpLister():
    def __init__(self, env=None):
        self.env = {}
        if env is not None:
            self.env = env
        self._tests = []
        skqp_dir = ProjectMap().build_root() + '/opt/skqp'
        out, err = run_batch_command([skqp_dir + '/skqp', '--gtest_list_tests',
                                      skqp_dir,
                                      ProjectMap().build_root() + '/tests'],
                                     streamedOutput=False, quiet=True,
                                     env=self.env)
        subgroup = None
        for test in out.split('\n'):
            if not test:
                continue
            if test.endswith('.'):
                subgroup = test
                continue
            if subgroup:
                test = test.lstrip()
                if test not in self._tests:
                    self._tests.append(subgroup + test)

    def filter(self, blacklist):
        if str == type(blacklist):
            blacklist = [blacklist]
        if isinstance(type(blacklist), type(SkqpLister)):
            blacklist = blacklist._tests
        for test in blacklist:
            if not test:
                continue
            if test.startswith("#"):
                continue
            if test in self._tests:
                self._tests.remove(test)

    def whitelist(self, whitelist):
        for test in self._tests:
            if test not in whitelist:
                self._tests.remove(test)

    def add(self, testlist):
        self._tests.extend(testlist._tests)

    def list(self):
        return self._tests

    def getone(self):
        return self._tests.pop()

    def count(self):
        return len(self._tests)

    def remove(self, test):
        if test in self._tests:
            self._tests.remove(test)


def process_test_result(result_elementtree, config, missing_revisions):
    """ When given a test result elementtree, return an updated elementtree
    to indicate test is skipped if the given config indicates that it
    should be. """
    expected_status = {}
    changed_commit = {}
    new_et = result_elementtree
    c = ConfigParser.SafeConfigParser(allow_no_value=True)
    c.read(config)
    for section in c.sections():
        for (test, commit) in c.items(section):
            if test in expected_status:
                raise Exception("test has multiple entries: " + test)
            expected_status[test] = section
            changed_commit[test] = commit
    for atest in new_et.findall(".//testcase"):
        test_name = (atest.attrib["classname"].lower() + '.'
                     + atest.attrib["name"].lower())
        # remove suffix
        test_name = '.'.join(test_name.split('.')[:-1])
        if atest.attrib["status"] == "lost":
            atest.attrib["status"] = "crash"
        if test_name not in expected_status:
            continue

        expected = expected_status[test_name]
        test_is_stale = False
        for missing_commit in missing_revisions:
            if missing_commit in changed_commit[test_name]:
                test_is_stale = True
                # change stale test status to skip
                for ftag in atest.findall("failure"):
                    atest.remove(ftag)
                for ftag in atest.findall("error"):
                    atest.remove(ftag)
                atest.append(et.Element("skipped"))
                so = et.Element("system-out")
                so.text = "WARN: the results of this were changed by " + changed_commit[test_name]
                so.text += ", which is missing from this build."
                atest.append(so)
                break
        if test_is_stale:
            continue
        if expected == "expected-failures":
            # change fail to pass
            if atest.attrib["status"] == "fail":
                for ftag in atest.findall("failure"):
                    atest.remove(ftag)
                so = et.Element("system-out")
                so.text = "Passing test as an expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "crash":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test crashed when it expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "pass":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test passed when it expected failure"
                atest.append(so)
            elif atest.attrib["status"] == "skip":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test skipped when it expected failure"
                atest.append(so)
            else:
                raise Exception("test has unknown status: " + atest.attrib["name"]
                                + " " + atest.attrib["status"])
        elif expected == "expected-crashes":
            # change error to pass
            if atest.attrib["status"] == "crash":
                for ftag in atest.findall("error"):
                    atest.remove(ftag)
                so = et.Element("system-out")
                so.text = "Passing test as an expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "fail":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test failed when it expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "pass":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test passed when it expected crash"
                atest.append(so)
            elif atest.attrib["status"] == "skip":
                atest.append(et.Element("failure"))
                so = et.Element("system-out")
                so.text = "ERROR: this test skipped when it expected crash"
                atest.append(so)
            else:
                raise Exception("test has unknown status: " + atest.attrib["name"]
                                + " " + atest.attrib["status"])
    return new_et


def skqp_get_deps_from_file(file_path):
    """ Return dict of dependencies listed in skqp DEPS file at
    given file_path """
    deps = {}
    execfile(file_path, deps)
    return deps['deps'].copy()


def get_external_revisions(revisions_dict=None):
    return skqp_external_revisions(project="skqp",
                                   revisions_dict=revisions_dict)


class SkqpBuilder():

    def __init__(self, extra_definitions=None, install=True):
        self._options = Options()
        self._pm = ProjectMap()
        self._install = install
        self._extra_definitions = extra_definitions or []
        self.tests = []  # List of tests to run
        self.gtests = ['skqp']

        self.project = self._pm.current_project()

        self._src_dir = self._pm.project_source_dir(self.project)
        self._build_root = self._pm.build_root()
        self._build_dir = os.path.join(
            self._src_dir, '_'.join(['build', self.project,
                                     self._options.arch]))

    def build(self):
        rmtree(self._src_dir + "/third-party")
        returnto = os.getcwd()
        dest = self._build_root + '/opt/skqp'
        assets = (self._pm.project_source_dir()
                  + '/../skqp_assets')
        if not os.path.exists(dest):
            os.makedirs(dest)
        os.chdir(self._src_dir)

        # Patch in local mirror of dependencies rather than fetching them
        # externally. Dependencies should be placed in 'repos' dir by
        # fetch_sources (or something similar).
        # Use skqp's git-sync-deps script after patching DEPS to pull in
        # dependencies.
        deps_file = self._src_dir + '/DEPS'
        deps_patched = False
        with open(deps_file, 'r') as f:
            deps_patched = f.readlines()[0].startswith('#patched')
        if not deps_patched:
            deps = skqp_get_deps_from_file(deps_file)
            for orig_dep_name, url in deps.items():
                dep_name = orig_dep_name.split('/')[-1]
                # some deps are in the local repos under slightly different
                # names..
                if dep_name in external_to_repo:
                    dep_name = external_to_repo[dep_name]
                deps[orig_dep_name] = (self._pm.project_source_dir()
                                       + '/../'
                                       + dep_name
                                       + '@' + url.split('@')[1])
            with open(deps_file, 'w') as f:
                f.write('#patched\n')
                f.write('use_relative_paths = True\n')
                f.write('deps = ' + json.dumps(deps) + '\n')
                f.write('\nrecursedeps = [ "common" ]')
        revisions = get_external_revisions()
        external_dir = (self._src_dir + "/third-party/external/{}/")
        checkout_externals(project='skqp', revisions=revisions,
                           external_dir_format=external_dir)
        # Nasty hack to prevent downloading an external binary..
        gn_dest = self._src_dir + '/buildtools/linux64/gn'
        # remove gn if it exists in the destination so fetch-gn doesn't fail
        if os.path.exists(gn_dest):
            os.remove(gn_dest)
        with open(self._src_dir + '/bin/fetch-gn', 'w') as f:
            f.write(("import os\n"
                     "os.symlink('{}','{}')"
                     "").format((self._src_dir
                                 + '/../skqp_assets/bin/gn'),
                                gn_dest))
        # This script copies dependency repos to third-party/external
        run_batch_command(['python', 'tools/git-sync-deps'])
        run_batch_command(['python', 'tools/skqp/setup_resources'])
        # apply patches if they exist
        for patch in sorted(glob.glob(os.path.join(
                            self._pm.project_build_dir(),
                            '*.patch'))):
            try:
                run_batch_command(['git', 'apply', patch])
            except subprocess.CalledProcessError:
                print('WARN: failed to apply patch: {}'.format(patch))

        cmd = ' '.join([self._src_dir + '/buildtools/linux64/gn', 'gen',
                        self._build_dir,
                        ' --args=\'cc=\"ccache clang\" '
                        + 'cxx=\"ccache clang++\"\''])
        if os.path.exists(self._build_dir):
            rmtree(self._build_dir)
        run_batch_command(cmd, shell=True)
        run_batch_command(['ninja', '-j', str(cpu_count()), '-C',
                           self._build_dir, 'skqp'])
        run_batch_command(['rsync', '-rlptD', self._build_dir + '/', dest])
        # install assets
        run_batch_command(['rsync', '-rlptD', assets + '/', dest + '/assets'])
        # install resources (needed by some tests)
        run_batch_command(['rsync', '-rlptD', self._src_dir + '/resources', dest])

        os.chdir(returnto)

        Export().export()

    def clean(self):
        git_clean(self._src_dir)
        rmtree(self._build_dir)
        assert not os.path.exists(self._build_dir)

    def test(self):
            pass


class SkqpTester():

    def __init__(self, env=None):
        self._options = Options()
        self._pm = ProjectMap()
        self.tests = []  # List of tests to run
        self.gtests = ['skqp']
        self.env = {}
        if env is not None:
            self.env = env

        self.project = self._pm.current_project()

        self._src_dir = self._pm.project_source_dir(self.project)
        self._build_root = self._pm.build_root()
        long_revisions = RepoSet().branch_missing_revisions()
        self.missing_revisions = [a_rev[:6] for a_rev in long_revisions]

    def test(self):
        # SKQP does not run reliably on anything older than Mesa 19.0
        if '18.' in mesa_version():
            return
        self.env.update(os.environ)
        self.env.update({
            'LD_LIBRARY_PATH': ':'.join([
                get_libdir(),
                os.path.join(get_libdir(), 'dri'),
            ]),
            "LIBGL_DRIVERS_PATH": get_libgl_drivers(),
        })
        results_dir = os.path.abspath(os.path.join(
            self._pm.build_root(), '../test'))
        if not os.path.exists(results_dir):
            os.makedirs(results_dir)
        self.tests = SkqpLister(env=self.env)
        gtest_path = self._build_root + '/opt/skqp'
        try:
            conf_file = get_conf_file(self._options.hardware,
                                      self._options.arch,
                                      project=self.project)
        except NoConfigFile:
            print(('No config file found for hardware: {0} '
                   'arch: {1}').format(self._options.hardware,
                                       self._options.arch),
                  file=sys.stderr)
            sys.exit(1)
        conf_dir = self._pm.source_root() + "/skqp-test/"
        blacklist = [line.rstrip() for line in open(conf_dir
                                                    + "blacklist.conf")]
        self.tests.filter(blacklist)
        if self._options.arch == "m32":
            blacklist = [line.rstrip() for line in open(conf_dir
                                                        + "m32_blacklist.conf")]
            self.tests.filter(blacklist)

        platform_blacklist = (conf_dir + self._options.hardware
                              + "_blacklist.conf")
        if os.path.exists(platform_blacklist):
            blacklist = [line.rstrip() for line in open(platform_blacklist)]
            self.tests.filter(blacklist)

        root = et.Element('testsuites')
        suite = et.SubElement(
            root,
            'testsuite',
            name=self._pm.current_project(),
            tests=str(len(self.tests.list())))

        filename = os.path.join(
            results_dir, 'piglit_' + self._pm.current_project()
            + '_'.join(['-unittest', self._options.config, self._options.arch,
                        self._options.hardware]) + '.xml')
        proc_env = self.env
        # At least one skqp test performs shader compiles (GLPrograms)
        proc_env['MESA_GLSL_CACHE_DISABLE'] = 'true'
        completion_percentage = 0
        completed_tests = 0
        prev_completed_tests = 0
        sleep_time_ms = 0.0
        cpus = cpu_count()
        procs = dict.fromkeys(range(1, cpus + 1))
        remaining_test_count = self.tests.count()
        full_test_count = remaining_test_count
        # note: gtest_filter and gtest_ouput=xml are set later on
        cmd = [gtest_path + '/skqp',
               '--gtest_output=xml:',
               '--gtest_filter=', gtest_path,
               results_dir]
        while (full_test_count - completed_tests) > 0:
            for cpu in range(1, cpus + 1):
                if procs[cpu] is None:
                    try:
                        # This test generally takes longer than the rest of
                        # the tests combined, so start it first
                        if 'Skia_Unit_Tests.GLPrograms' in self.tests.list():
                            self.tests.remove('Skia_Unit_Tests.GLPrograms')
                            core_test = 'Skia_Unit_Tests.GLPrograms'
                        else:
                            core_test = self.tests.getone()
                    except IndexError:
                        # no more tests to add
                        break
                else:
                    # core already has a test
                    continue
                test_result_xml = '_'.join(['gtest', core_test,
                                            self._options.config,
                                            self._options.arch,
                                            self._options.hardware]) + '.xml'
                test_result_xml = os.path.join(results_dir, test_result_xml)
                # splice in test name and results file into the command list
                core_cmd = [cmd[0], cmd[1] + test_result_xml,
                            cmd[2] + core_test] + cmd[3:]
                err_fh = tempfile.TemporaryFile('w+')
                out_fh = tempfile.TemporaryFile('w+')
                proc = subprocess.Popen(core_cmd, env=proc_env, stdout=out_fh,
                                        stderr=err_fh)
                proc.test_result_xml = test_result_xml
                proc.test_name = core_test
                proc.out_fh = out_fh
                proc.err_fh = err_fh
                procs[cpu] = proc
            # Print progress
            new_percentage = (completed_tests * 100) / full_test_count
            if new_percentage > completion_percentage:
                completion_percentage = new_percentage
                print("[ " + str(completion_percentage) + "% ]")
            # No tests completed when we polled.  Delay to
            # avoid a spin loop.
            # This uses a backoff timer since some tests are very fast but
            # some may take a few seconds or more to complete
            if prev_completed_tests == completed_tests:
                time.sleep(sleep_time_ms / 1000.0)
                if sleep_time_ms < 10.0:
                    sleep_time_ms = 10.0
                elif sleep_time_ms <= 5000:
                    sleep_time_ms *= 2.0
            else:
                prev_completed_tests = completed_tests
                sleep_time_ms = 0.0
            # Process completed test(s)
            for cpu, proc in procs.items():
                if proc is None:
                    continue
                proc.poll()
                if proc.returncode is None:
                    continue
                test_result_xml = proc.test_result_xml

                proc.err_fh.seek(0)
                proc.out_fh.seek(0)
                try:
                    ts_xml = et.parse(test_result_xml)
                    # skqp does not set status tag to anything useful for us,
                    # so try to determine if this was a pass or fail and
                    # set the status tag accordingly so filtering works
                    for testcase in ts_xml.findall('.//testcase'):
                        if testcase.find('.//failure') is None:
                            testcase.set('status', 'pass')
                        else:
                            testcase.set('status', 'fail')
                            system_err = et.SubElement(testcase, 'system-err')
                            system_err.text = ''.join(proc.err_fh.readlines())
                        # Add platform/arch suffix to test
                        name = (testcase.get('name') + '.'
                                + self._options.hardware + self._options.arch)
                        testcase.set('name', name)
                        system_out = et.SubElement(testcase, 'system-out')
                        system_out.text = ''.join(proc.out_fh.readlines())
                except (IOError, et.ParseError):
                    ts_xml = Export().create_failing_test_xml("failing-gtest-"
                                                              + proc.test_name,
                                                              "ERROR: gtest "
                                                              "crashed "
                                                              + proc.test_name)
                    testcase = ts_xml.find('.//testcase')
                    system_err = et.SubElement(testcase, 'system-err')
                    system_err.text = ''.join(proc.err_fh.readlines())
                    system_out = et.SubElement(testcase, 'system-out')
                    system_out.text = ''.join(proc.out_fh.readlines())

                proc.err_fh.close()
                proc.out_fh.close()
                ts_xml = process_test_result(ts_xml, conf_file,
                                             self.missing_revisions)
                for testcase in ts_xml.findall('.//testcase'):
                    suite.append(testcase)
                procs[cpu] = None
                completed_tests += 1
                # test results are summarized into one xml at the end, so
                # individual test xml is no longer needed
                if os.path.exists(test_result_xml):
                    os.remove(test_result_xml)
        # save all results
        tree = et.ElementTree(root)
        tree.write(filename, encoding='utf-8', xml_declaration=True)
        Export().export_tests()

    def build(self):
        pass

    def clean(self):
        pass
