import urllib2
import glob
import sys
import os
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(sys.argv[0])), ".."))
import build_support as bs

import_url = ("http://otc-mesa-ci.jf.intel.com/job/ImportResults/buildWithParameters?"
              "token=noauth&"
              "url={0}".format(urllib2.quote(os.environ["BUILD_URL"])))

print "triggering: " + import_url
urllib2.urlopen(import_url)

